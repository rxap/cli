@rxap/cli
=================

ReactiveX Application Platform cli tool


[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@rxap/cli.svg)](https://npmjs.org/package/@rxap/cli)
[![Downloads/week](https://img.shields.io/npm/dw/@rxap/cli.svg)](https://npmjs.org/package/@rxap/cli)


<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @rxap/cli
$ rxap COMMAND
running command...
$ rxap (--version)
@rxap/cli/0.2.1 linux-x64 node-v20.9.0
$ rxap --help [COMMAND]
USAGE
  $ rxap COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`rxap compose apply`](#rxap-compose-apply)
* [`rxap help [COMMAND]`](#rxap-help-command)
* [`rxap workspace migrate [VERSION]`](#rxap-workspace-migrate-version)

## `rxap compose apply`

Run the generate for all specified schematic files

```
USAGE
  $ rxap compose apply [--feature <value> --project <value>] [--filter <value>] [--overwrite <value>]
    [--overwriteAll] [-v]

FLAGS
  -v, --verbose            Verbose output
      --feature=<value>    Name of a feature in a project
      --filter=<value>     Folder filter to apply
      --overwrite=<value>  Overwrite type
      --overwriteAll       Overwrite all files
      --project=<value>    Name of a project

DESCRIPTION
  Run the generate for all specified schematic files

EXAMPLES
  $ rxap compose apply
```

_See code: [src/commands/compose/apply.ts](https://gitlab.com/rxap/cli/blob/v0.2.1/src/commands/compose/apply.ts)_

## `rxap help [COMMAND]`

Display help for rxap.

```
USAGE
  $ rxap help [COMMAND...] [-n]

ARGUMENTS
  COMMAND...  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for rxap.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v6.2.3/src/commands/help.ts)_

## `rxap workspace migrate [VERSION]`

Migrate the workspace to the specified version of the ReactiveX Application Platform

```
USAGE
  $ rxap workspace migrate [VERSION] [-a] [-v]

ARGUMENTS
  VERSION  [default: latest] Version to migrate to

FLAGS
  -a, --auto     Automatically update and upgrade the workspace
  -v, --verbose  Verbose output

DESCRIPTION
  Migrate the workspace to the specified version of the ReactiveX Application Platform

EXAMPLES
  $ rxap workspace migrate
```

_See code: [src/commands/workspace/migrate.ts](https://gitlab.com/rxap/cli/blob/v0.2.1/src/commands/workspace/migrate.ts)_
<!-- commandsstop -->
