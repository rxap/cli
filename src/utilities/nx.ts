import { spawn } from 'node:child_process';

export type CommandParameters = Record<string, boolean | number | string>;

function buildParametersString(args: CommandParameters) {
  return Object.entries(args).map(([ key, value ]) => {

    switch (typeof value) {
      case 'string': {
        return `--${ key }="${ value }"`;
      }

      case 'boolean': {
        return value ? `--${ key }` : `--no-${ key }`;
      }

      case 'number': {
        return `--${ key }=${ value }`;
      }

      default: {
        console.warn(`Unknown type for ${ key }: ${ typeof value }`);
        return `--${ key }=${ value }`;
      }
    }
  });
}

export function runNxCommand(args: string[], parameters: CommandParameters = {}, verbose = false) {
  const parametersString = buildParametersString(parameters);
  if (verbose) {
    console.log(`$ yarn nx ${ args.join(' ') } ${ parametersString.join(' ') }`);
  }

  return new Promise((resolve, reject) => {
    const s = spawn(
      'yarn',
      [
        'nx',
        ...args,
        ...parametersString
      ],
      {
        shell: true,
        stdio: [ 'ignore', 'pipe', 'inherit' ],
      },
    );
    s.on('error', (err: { code?: string } & Error) => {
      if (err.code === 'ENOENT') {
        console.error('Yarn must be installed to use the CLI.');
      } else {
        reject(err);
      }
    });
    s.stdout.on('data', (data: Buffer) => {
      console.log(data.toString('utf8'));
    });
    s.on('close', resolve);
  });
}
