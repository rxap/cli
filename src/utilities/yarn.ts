import { spawn } from 'node:child_process';

export function runYarnCommand(args: string[], verbose = false) {
  if (verbose) {
    console.log(`$ yarn ${ args.join(' ') } }`);
  }

  return new Promise((resolve, reject) => {
    const s = spawn(
      'yarn',
      [
        ...args,
      ],
      {
        shell: true,
        stdio: [ 'ignore', 'pipe', 'inherit' ],
      },
    );
    s.on('error', (err: { code?: string } & Error) => {
      if (err.code === 'ENOENT') {
        console.error('Yarn must be installed to use the CLI.');
      } else {
        reject(err);
      }
    });
    s.stdout.on('data', (data: Buffer) => {
      console.log(data.toString('utf8'));
    });
    s.on('close', resolve);
  });
}
