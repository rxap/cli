import {
  Command,
  Flags,
} from '@oclif/core';

import {
  CommandParameters,
  runNxCommand,
} from '../../utilities/nx.js';

export default class ComposeApply extends Command {
  static override args = {}

  static override description = 'Run the generate for all specified schematic files'

  static override examples = [
    '<%= config.bin %> <%= command.id %>',
  ]

  static override flags = {
    feature: Flags.string({dependsOn: ['project'], description: 'Name of a feature in a project'}),
    filter: Flags.string({ description: 'Folder filter to apply'}),
    overwrite: Flags.string({ description: 'Overwrite type' }),
    overwriteAll: Flags.boolean({ description: 'Overwrite all files'}),
    project: Flags.string({description: 'Name of a project'}),
    verbose: Flags.boolean({char: 'v', description: 'Verbose output'}),
  }

  public async run(): Promise<void> {
    const {flags} = await this.parse(ComposeApply)

    const parameters: CommandParameters = {};

    if (flags.project) {
      parameters.project = flags.project;
    }

    if (flags.feature) {
      parameters.feature = flags.feature;
    }

    if (flags.filter) {
      parameters.filter = flags.filter;
    }

    if (flags.overwrite) {
      parameters.overwrite = flags.overwrite;
    }

    if (flags.overwriteAll) {
      parameters.overwrite = true;
    }

    if (flags.verbose) {
      parameters.verbose = true;
    }

    await runNxCommand(['generate', `@rxap/schematic-composer:compose`], parameters, flags.verbose);
  }
}
