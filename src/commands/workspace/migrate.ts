import {
  Args,
  Command,
  Flags,
} from '@oclif/core';

import {
  CommandParameters,
  runNxCommand,
} from '../../utilities/nx.js';
import { runYarnCommand } from '../../utilities/yarn.js';

export default class WorkspaceMigrate extends Command {
  static override args = {
    version: Args.string({default: 'latest', description: 'Version to migrate to'}),
  }

  static override description = 'Migrate the workspace to the specified version of the ReactiveX Application Platform'

  static override examples = [
    '<%= config.bin %> <%= command.id %>',
  ]

  static override flags = {
    auto: Flags.boolean({ char: 'a', description: 'Automatically update and upgrade the workspace'}),
    verbose: Flags.boolean({char: 'v', description: 'Verbose output'}),
  }

  public async run(): Promise<void> {
    const {args, flags} = await this.parse(WorkspaceMigrate)

    const parameters: CommandParameters = {};
    if (flags.verbose) {
      parameters.verbose = true;
    }

    await runNxCommand(['migrate', `rxap@${args.version}`], parameters, flags.verbose);

    if (flags.auto) {
      await runYarnCommand(['install']);
      await runNxCommand(['run-many'], {
        target: 'lint',
        fix: true
      });
      await runNxCommand(['migrate'], {
        commitPrefix: 'chore: ',
        createCommits: true,
        ifExists: true,
        runMigrations: true
      });
    }

  }
}
